DROP TABLE IF EXISTS credentials;
CREATE TABLE credentials
(
    credID int NOT NULL auto_increment,
    user varchar(50) NOT NULL,
    passw varchar(6) NOT NULL,
    PRIMARY KEY (credID)
);

DROP TABLE IF EXISTS login;
CREATE TABLE login
(
    logID int NOT NULL auto_increment,
    user varchar(50) NOT NULL,
    passw varchar(6) NOT NULL,
    credID int,
    PRIMARY KEY (logID),
    FOREIGN KEY (credID) REFERENCES credentials(credID)
);